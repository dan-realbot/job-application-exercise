## Setup:
- clone this repo
- `npm install`
- `npm start` (`npm run dev` to watch for changes)
- visit `http://localhost:3000`
- done

----

### Links:
[Pannellum Docs](https://pannellum.org/documentation/overview/)

[Pannellum Repo](https://github.com/mpetroff/pannellum)

import { VirtualTour } from './virtualtour.js';


const tour = new VirtualTour('http://localhost:3000', true);

// Initialise viewer in a container (HTMLElement or Id of HTMLElement)
const tourContainer = document.getElementById('tour-container');
tour.init(tourContainer);

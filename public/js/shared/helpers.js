export const toggleOrientationControlButton = (has_control) => {
  const pnlm_orientation_button = document.querySelector('.pnlm-controls.pnlm-control.pnlm-orientation-button');

  if (!pnlm_orientation_button) {
    return;
  }

  if (has_control && pnlm_orientation_button.style.display !== 'block') {
    pnlm_orientation_button.style.display = 'block';
  }
  else if (!has_control && pnlm_orientation_button.style.display !== 'none') {
    pnlm_orientation_button.style.display = 'none';
  }
};

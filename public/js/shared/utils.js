export const preloadImage = (src) => {
  const image = new Image();
  image.src = src;

  return image;
};

export const fetchAndCreateObjectURL = (url) => {
  return fetch(url)
    .then(res => res.blob())
    .then(blob => URL.createObjectURL(blob));
};
